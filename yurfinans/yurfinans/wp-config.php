<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'yurfinans' );

/** MySQL database username */
define( 'DB_USER', 'yurfinans' );

/** MySQL database password */
define( 'DB_PASSWORD', 'yurfinans' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ekcmcM0[WsY+xEzMJIPNXB@<d!jDjlb*|LkgwKjMO%KPyreM8.q4fk#M7eQob.*U' );
define( 'SECURE_AUTH_KEY',  'T7/LGx(W]+%^yK]ld7,5UV2O?xYu-^^QkWG)W5)r;nI=.!>M(?`XQWcf7#;;0Lw]' );
define( 'LOGGED_IN_KEY',    'rYyG]FG4jGca[ewXVVOGpvWewG59|t3QQo%A;oq/jO?6L8pClHhgXBF(iL9bDMAP' );
define( 'NONCE_KEY',        '] p_t$7D:nnff?~W1=yCOiuID!8#RdxYQqY{FImc:1}_`W;$QPyx  `V?w;uvTGg' );
define( 'AUTH_SALT',        'sT,]J95d+u!!!igD^?CAQ#5e[~Fk|0>noTwfn<!.E6.>9vn?,Ai:?R]k33_6O0^/' );
define( 'SECURE_AUTH_SALT', '@d_-?F|QIn$ML8EbVX:UCOGVJ{I&X*wC%CD%*W%7P/,.+.QYQ[Cv>*qkbd[nK#i0' );
define( 'LOGGED_IN_SALT',   'q{OUycTbq}Y#J#t<H<oJ_*2p]>SMyNb E5.s4E Y?N#w*pq42T@jw8%kR=[~bLki' );
define( 'NONCE_SALT',       'n;SF=8FqJev2:Z1uvxlj_$]Nhh;lP5GzJ,ymeJy!E%}=$VwcSe?{P~,E0mCcM:rK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
