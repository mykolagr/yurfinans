

    <?php while ( have_rows('acf_page_components') ) : the_row(); 


        if( get_row_layout() == 'acf_pages_components_home_banner') { 

            get_template_part('components/pages/banner-home/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_banner_post') { 

            get_template_part('components/pages/banner-post/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_service') { 

            get_template_part('components/pages/service/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_service_cost_table') { 

            get_template_part('components/pages/service-cost-table/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_service_cost') { 

            get_template_part('components/pages/service-cost/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_head_post') { 

            get_template_part('components/pages/head-post/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_faq') { 

            get_template_part('components/pages/faq/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_ours_clients') { 

            get_template_part('components/pages/ours-clients/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_description') { 

            get_template_part('components/pages/description/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_faq_decore') { 

            get_template_part('components/pages/faq-decore/component'); 
 
        } 

        if( get_row_layout() == 'acf_pages_components_exclusive') { 

            get_template_part('components/pages/exclusive/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_advice') { 

            get_template_part('components/pages/advice/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_values') { 

            get_template_part('components/pages/values/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_teams') { 

            get_template_part('components/pages/teams/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_letter') { 

            get_template_part('components/pages/letter/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_banner-practic') { 

            get_template_part('components/pages/banner-practic/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_publication') { 

            get_template_part('components/pages/publication/component'); 
 
        }

        if( get_row_layout() == 'acf_pages_components_calculation') { 

            get_template_part('components/pages/calculation/component'); 
 
        }
    endwhile; ?>


