<section class="section section-banner section-banner--radiators">
  <div class="container">
    <div class="banner-main">
      <div class="banner-main__top">
        <div class="banner-main__top-ttl">
          <div class="breadcrumbs">
            <div class="breadcrumbs__inner">
              <ul itemscope="itemscope" itemtype="http://schema.org/BreadcrumbList" class="h-reset-list breadcrumbs__list">
                <li itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem"><a itemprop="item" href="main.html"><span itemprop="name">home</span>
                    <svg width="1em" height="1em" class="icon icon-sm-arrow-right ">
                      <use xlink:href="s/images/useful/svg/theme/symbol-defs.svg#icon-sm-arrow-right"></use>
                    </svg></a>
                  <meta itemprop="position" content="1"/>
                </li>
                <li itemprop="itemListElement" itemscope="itemscope" itemtype="http://schema.org/ListItem"><span itemprop="name">Radiators</span>
                  <meta itemprop="position" content="2"/>
                </li>
              </ul>
            </div>
          </div>

          <?php if(get_sub_field('acf_page_component_banner_title')){?>

            <h1 class="h1">
                <?php echo get_sub_field('acf_page_component_banner_title');?>
            </h1>

            <?php }?>
            <?php $banner_icon_1_ids = get_sub_field('acf_page_component_banner_icon_logo');

            $banner_icon_1_args = array(
                'post_type' => 'icons',
                'posts_per_page' => 1,
                'order'          => 'ASC',
                'post_status'    => 'publish',
                'post__in'       =>  $banner_icon_1_ids,
                'orderby'        => 'post__in',
            );

            $banner_icons_1 = get_posts($banner_icon_1_args); 
                  
                  foreach( $banner_icons_1 as $banner_icon_1 ):  ?>
                  <svg width="1em" height="1em" class="icon icon-heading-decor ">
                    <use xlink:href="<?=THEME?>/dist/s/images/useful/svg/theme/symbol-defs.svg#<?php echo get_field('acf_icon_id', $banner_icon_1); ?>"></use>
                  </svg>

                <?php endforeach; ?>
            
            <?php wp_reset_postdata(); ?>
         
        </div>
        <div class="banner-main__top-img">
          <div class="h-object-fit">
            <picture>
              <?php $banner_image = get_sub_field('acf_page_component_banner_image');
                if( !empty($banner_image) ): ?>
                  <img src="<?php echo $banner_image['url']; ?>" alt="<?php echo $banner_image['alt']; ?>" />
              <?php endif; ?>
            </picture>
          </div>
        </div>
      </div>
      <div class="banner-main__article">
        <?php if(get_sub_field('acf_page_component_banner_subtitle')): ?>
            <h2 class="h2">
              <?php echo get_sub_field('acf_page_component_banner_subtitle'); ?>
            </h2>
        <?php endif; ?>
        <?php if(get_sub_field('acf_page_component_banner_excerpt')): ?>
              <p>
                <?php echo get_sub_field('acf_page_component_banner_excerpt'); ?>
              </p>
        <?php endif; ?>

        <?php $banner_icon_2_ids = get_sub_field('acf_page_component_banner_icon_paladin');
            $banner_icon_2_args = array(
                'post_type' => 'icons',
                'posts_per_page' => 1,
                'order'          => 'ASC',
                'post_status'    => 'publish',
                'post__in'       =>  $banner_icon_2_ids,
                'orderby'        => 'post__in',
            );

            $banner_icons_2 = get_posts($banner_icon_2_args); 
                    
                foreach( $banner_icons_2 as $banner_icon_2 ):  ?>
                <svg width="1em" height="1em" class="icon icon-logo-2">
                  <use xlink:href="<?=THEME?>/dist/s/images/useful/svg/theme/symbol-defs.svg#<?php echo get_field('acf_icon_id', $banner_icon_2); ?>"></use>
                </svg>

              <?php endforeach; ?>
          <?php wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</section>