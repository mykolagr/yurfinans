<?php

register_nav_menus(array(
	'header-top' => 'Header-top menu',
));

function header_top_menu(){
	wp_nav_menu( array(
		'theme_location' => 'header-top-menu',
        'container' => false,
	) );
}

register_nav_menus(array(
	'header-primery' => 'Header-primery menu',
));

function header_primery_menu(){
	wp_nav_menu( array(
		'theme_location' => 'header-primery-menu',
        'container' => false,
	) );
}

// footer-menu-col-1
register_nav_menus(array(
	'footer-menu-col-1' => 'Footer Menu - column 1',
));

function footer_menu_col_1(){
	wp_nav_menu( array(
		'theme_location' => 'footer-menu-col-1',
        'container'        => false,
		'items_wrap'      => '<ul class="js-collapse-content">%3$s</ul>',
	) );
}

// footer-menu-col-2
register_nav_menus(array(
	'footer-menu-col-2' => 'Footer Menu - column 2',
));

function footer_menu_col_2(){
	wp_nav_menu( array(
		'theme_location' => 'footer-menu-col-2',
        'container'        => false,
		'items_wrap'      => '<ul class="js-collapse-content">%3$s</ul>',
	) );
}



//  header menu style


//menu ul 
add_filter( 'wp_nav_menu_args', 'filter_nav_menu_args' );
function filter_nav_menu_args( $args ) {
	if ( $args['theme_location'] === 'header-menu' ) {
		$args['container']  = false;
		$args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
		$args['menu_class'] = 'header__menu-content-nav-list';
	}
	return $args;
}


// header-menu ul li
add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( $args->theme_location === 'header-menu' ) {
            $classes['class'] = 'header__menu-content-nav-li';
	}
	return $classes;
}



//menu ul li a
function add_specific_menu_location_atts( $atts, $item, $args ) {

    if( $args->theme_location == 'header-menu' ) {
            $atts['class'] = 'header__menu-content-nav-link';
    }
    return $atts;
    
}
add_filter( 'nav_menu_link_attributes', 'add_specific_menu_location_atts', 10, 3 );










// footer style


//menu ul 
add_filter( 'wp_nav_menu_args', 'filter_nav_footer_menu_args' );
function filter_nav_footer_menu_args( $args ) {
	if ( $args['theme_location'] === 'footer-menu' ) {
		$args['container']  = false;
		$args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
		$args['menu_class'] = 'c-footer-menu__items h-reset-list'; 
	}
	return $args;
}


// footer-menu ul li
add_filter( 'nav_menu_css_class', 'filter_nav_footer_menu_css_classes', 10, 4 );
function filter_nav_footer_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( $args->theme_location === 'footer-menu' ) {
            $classes = ['c-footer-menu__link'];
	}
	return $classes;
}



//menu ul li a
function add_specific_footer_menu_location_atts( $atts, $item, $args ) {

    if( $args->theme_location == 'footer-menu' ) {
            $atts['class'] = 'c-footer-menu__link';
    }
    return $atts;
    
}
add_filter( 'nav_menu_link_attributes', 'add_specific_footer_menu_location_atts', 10, 3 );