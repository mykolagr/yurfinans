<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta name="robots" content="noindex" />
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <title><?php wp_title("", true); ?></title>
<?php wp_head(); ?>

  </head>
  <body <?php body_class(); ?>>
